$(document).ready(function(){
    $(document).scroll(function(){
      if($(document).scrollTop() >= 100){
        $("nav").slideUp(1000)
      }
      if($(document).scrollTop() == 0){
        $("nav").slideDown(1000)
      }
    });

    $(".js-border").each(function(){
      $(this).mouseenter(function(){
        if($(".border-js-center").hasClass("border-js-center")) {
          $(".border-js-center").hide(700);
        }
        if($(".border-js-right").hasClass("border-js-right")) {
          $(".border-js-right").hide(700);
        }
      });
      $(this).mouseleave(function(){
        if($(".border-js-center").hasClass("border-js-center")) {
          $(".border-js-center").show(700);
        }
        if($(".border-js-right").hasClass("border-js-right")) {
          $(".border-js-right").show(700);
        }
      });
    });

});
